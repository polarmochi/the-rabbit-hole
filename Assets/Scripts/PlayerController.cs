using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float maxJumpStrength = 500f;
    public float jumpCooldown = .5f;
    public Transform forceRangePoint;
    
    private Rigidbody2D rb;
    private Collider2D bunCollider;
    private Camera mainCamera;
    private int terrainMask;

    private float maxForceDistance;
    private float jumpResetTime = 0f;
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        bunCollider = GetComponent<Collider2D>();
        mainCamera = Camera.main;
        terrainMask = LayerMask.GetMask("Terrain");
        maxForceDistance = Vector3.Distance(forceRangePoint.position, transform.position);
        print("Max Force Distance: " + maxForceDistance);
    }

    void Update()
    {
        if (!ReadyToJump())
        {
            jumpResetTime -= Time.deltaTime;
        }
    }

    private void FixedUpdate()
    {
        if (Input.GetMouseButton(0) && ReadyToJump() && bunCollider.IsTouchingLayers(terrainMask))
        {
            rb.AddForce(GetJumpForce());
            jumpResetTime = jumpCooldown;
        }
    }

    private bool ReadyToJump()
    {
        return jumpResetTime <= 0f;
    }
    
    void OnCollisionEnter2D(Collision2D col)
    {
        //todo: check for powerups
    }

    private Vector2 GetJumpForce()
    {
        Vector3 mousePos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0f;
        Vector3 forceDirection = mousePos - transform.position;
        Vector2 normalForce = new Vector2(forceDirection.x, Mathf.Abs(forceDirection.y));
        normalForce.Normalize();
        return normalForce * GetJumpStrength(mousePos);
    }

    private float GetJumpStrength(Vector3 mousePos)
    {
        float cursorDist = Vector3.Distance(mousePos, transform.position);
        float percentStrength = cursorDist / maxForceDistance;
        if (percentStrength > 1f)
        {
            percentStrength = 1f;
        }
        print("Percent Strength: " + percentStrength);
        return maxJumpStrength * percentStrength;
    }
}
