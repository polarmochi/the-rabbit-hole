using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowMouse : MonoBehaviour
{
    public GameObject reticleSprite;

    Transform reticleTransform;
    new Camera camera;
    
    // Start is called before the first frame update
    void Start()
    {
        reticleTransform = reticleSprite.transform;
        camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = camera.ScreenToWorldPoint(Input.mousePosition);
        reticleTransform.position = new Vector3(mousePos.x, mousePos.y, 0);
    }
}
